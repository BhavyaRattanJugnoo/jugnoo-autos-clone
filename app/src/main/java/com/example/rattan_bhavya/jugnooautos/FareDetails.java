package com.example.rattan_bhavya.jugnooautos;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.TextView;

import rmn.androidscreenlibrary.ASSL;


public class FareDetails extends Activity {

    TextView tv_fare_details;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fare_details);

     new ASSL(this, (ViewGroup)findViewById(R.id.root),1134,720,false);

        tv_fare_details=(TextView)findViewById(R.id.tv_fare_details);
        tv_fare_details.setText(Html.fromHtml(getString(R.string.text_fare_details)));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ASSL.closeActivity(findViewById(R.id.root));
    }
}
