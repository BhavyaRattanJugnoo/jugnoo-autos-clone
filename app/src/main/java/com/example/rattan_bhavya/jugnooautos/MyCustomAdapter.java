package com.example.rattan_bhavya.jugnooautos;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import rmn.androidscreenlibrary.ASSL;

/**
 * Created by RATTAN-BHAVYA on 3/9/2015.
 */
public class MyCustomAdapter extends BaseAdapter {
    String[] result;
    Context context;
    int[] imageId;
    Holder holder;
    private static LayoutInflater inflater = null;

    public MyCustomAdapter(Home home, String[] menuItems, int[] menuImages) {
        // TODO Auto-generated constructor stub
        result = menuItems;
        context = home;
        imageId = menuImages;
        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return result.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder {
        TextView tv;
        ImageView img;
        LinearLayout relative;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub


        if (convertView == null) {
            holder = new Holder();
            convertView = inflater.inflate(R.layout.drawer_list_item, null);
            holder.tv = (TextView) convertView.findViewById(R.id.tv_drawer_list_item);
            holder.img = (ImageView) convertView.findViewById(R.id.img_drawer_list_item);

            holder.relative = (LinearLayout) convertView.findViewById(R.id.layout_drawer_list_item);
            holder.relative.setTag(holder);
            holder.relative.setLayoutParams(new ListView.LayoutParams(720, LinearLayout.LayoutParams.WRAP_CONTENT));

            ASSL.DoMagic(holder.relative);
            convertView.setTag(holder);

        } else {

            holder = (Holder) convertView.getTag();

        }
        if (position == 0) {
            holder.tv.setText(result[position]);
            holder.img.setImageResource(imageId[position]);

        } else {
            holder.tv.setText(result[position]);
        }

        return convertView;
    }
}



