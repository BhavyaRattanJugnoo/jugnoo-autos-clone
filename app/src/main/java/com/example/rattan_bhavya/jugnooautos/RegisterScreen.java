package com.example.rattan_bhavya.jugnooautos;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import rmn.androidscreenlibrary.ASSL;


public class RegisterScreen extends Activity {

    TextView tvBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_screen);
        new ASSL(this, (ViewGroup) findViewById(R.id.root), 1134, 720, false);

        tvBack = (TextView) findViewById(R.id.tv_back);

        tvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_register:
                Intent intentOtpScreen = new Intent(RegisterScreen.this, OtpScreen.class);
                startActivity(intentOtpScreen);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ASSL.closeActivity(findViewById(R.id.root));
    }
}
