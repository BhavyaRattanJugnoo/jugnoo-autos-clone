package com.example.rattan_bhavya.jugnooautos;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import rmn.androidscreenlibrary.ASSL;


public class Home extends Activity implements LocationListener {
    static Marker marker;
    // Google Map
    private GoogleMap googleMap;
    ImageView imgProfilePic;

    ListView lv;
    Context context;

    ArrayList listMenuItems;
    public static int[] menuImages = {R.drawable.free_rides_icon};
    public static String[] menuItems = {"Coupons", "Invite and Get FREE Rides", "Rides", "Fare Details", "Help", "Logout"};

    DrawerLayout homeDrawer;
    LinearLayout layoutOverMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        new ASSL(this, (ViewGroup) findViewById(R.id.drawer_layout), 1134, 720, false);

        homeDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        layoutOverMap = (LinearLayout) findViewById(R.id.layout_over_map);
        imgProfilePic = (ImageView) findViewById(R.id.img_profile_pic);

        Picasso.with(Home.this).load("http://s5.postimg.org/myblxhg47/default_profile_pic.jpg").transform(new RoundedTransformation(270, 0)).into(imgProfilePic);

        lv = (ListView) findViewById(R.id.list_menu);
        lv.setAdapter(new MyCustomAdapter(this, menuItems, menuImages));

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                switch (position) {
                    case 0:
                        Intent coupons = new Intent(Home.this, Coupons.class);
                        startActivity(coupons);
                        break;
                    case 1:
                        Intent share = new Intent(Home.this, Share.class);
                        startActivity(share);

                        break;
                    case 2:
                        Intent rides = new Intent(Home.this, Rides.class);
                        startActivity(rides);

                        break;
                    case 3:
                        Intent fareDetails = new Intent(Home.this, FareDetails.class);
                        startActivity(fareDetails);

                        break;
                    case 4:
                        Intent help = new Intent(Home.this, Help.class);
                        startActivity(help);

                        break;
                    case 5:
                        Toast.makeText(Home.this, "You Clicked logout", Toast.LENGTH_LONG).show();
                        break;
                    default:
                        break;
                }
            }
        });
        homeDrawer.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {
            }

            @Override
            public void onDrawerClosed(View drawerView) {
            }

            @Override
            public void onDrawerStateChanged(int newState) {


                if (homeDrawer.isDrawerOpen(GravityCompat.START)) {
                    //drawer is open
                    layoutOverMap.setVisibility(View.VISIBLE);
                } else if (!homeDrawer.isDrawerOpen(GravityCompat.START)) {
                    layoutOverMap.setVisibility(View.INVISIBLE);
                }
            }
        });
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());

        // Showing status
        if (status != ConnectionResult.SUCCESS) { // Google Play Services are not available

            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();

        } else { // Google Play Services are available

            // Getting reference to the SupportMapFragment of activity_main.xml
            MapFragment fm = (MapFragment) getFragmentManager().findFragmentById(R.id.map);

            // Getting GoogleMap object from the fragment
            googleMap = fm.getMap();

            // Enabling MyLocation Layer of Google Map
            googleMap.setMyLocationEnabled(true);

            googleMap.getUiSettings().setZoomControlsEnabled(false);
            // Getting LocationManager object from System Service LOCATION_SERVICE
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

            // Creating a criteria object to retrieve provider
            Criteria criteria = new Criteria();

            // Getting the name of the best provider
            String provider = locationManager.getBestProvider(criteria, true);

            // Getting Current Location
            Location location = locationManager.getLastKnownLocation(provider);

            if (location != null) {
                onLocationChanged(location);
            }
            locationManager.requestLocationUpdates(provider, 20000, 0, Home.this);
        }
    }

    @Override
    public void onLocationChanged(Location location) {


        // Getting latitude of the current location
        double latitude = location.getLatitude();

        // Getting longitude of the current location
        double longitude = location.getLongitude();

        // Creating a LatLng object for the current location
        LatLng latLng = new LatLng(latitude, longitude);


        // Showing the current location in Google Map
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

        // Zoom in the Google Map
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));


        if (marker != null) {
            marker.remove();
        }
// create marker
        MarkerOptions markerOptionObject = new MarkerOptions().position(latLng).title("You");

// Changing marker icon
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 2;
        Bitmap scaledPinBall = BitmapFactory.decodeResource(getResources(), R.drawable.pin_ball, options);
        markerOptionObject.icon(BitmapDescriptorFactory.fromBitmap(scaledPinBall));

// adding marker
        marker = googleMap.addMarker(markerOptionObject);


    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {


    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public void onClick(View view) {

        homeDrawer.openDrawer(Gravity.START);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        ASSL.closeActivity(findViewById(R.id.drawer_layout));
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_MENU) {
            homeDrawer.openDrawer(Gravity.START);
        }
        return super.onKeyDown(keyCode, event);

    }
}
