package com.example.rattan_bhavya.jugnooautos;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;

import rmn.androidscreenlibrary.ASSL;


public class Help extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        new ASSL(this,(ViewGroup)findViewById(R.id.root),1134,720,false);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        ASSL.closeActivity(findViewById(R.id.root));
    }
}
