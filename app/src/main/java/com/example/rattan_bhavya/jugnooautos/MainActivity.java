package com.example.rattan_bhavya.jugnooautos;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;

import rmn.androidscreenlibrary.ASSL;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        new ASSL(this, (ViewGroup) findViewById(R.id.root), 1134, 720, false);

        final ImageView logoImage = (ImageView) findViewById(R.id.logo_image);
        final ImageView logoText = (ImageView) findViewById(R.id.logo_text);
        final Button btnLogin = (Button) findViewById(R.id.btn_login);
        final Button btnRegister = (Button) findViewById(R.id.btn_register);

        TranslateAnimation tAnimation = new TranslateAnimation(0, 0, -300, 40);
        tAnimation.setDuration(1000);
        tAnimation.setRepeatCount(0);
        tAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        tAnimation.setFillAfter(true);
        tAnimation.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // TODO Auto-generated method stub

                logoText.setVisibility(View.VISIBLE);
                btnLogin.setVisibility(View.VISIBLE);
                btnRegister.setVisibility(View.VISIBLE);
            }
        });

        logoImage.startAnimation(tAnimation);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ASSL.closeActivity(findViewById(R.id.root));
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_login:
                Intent intentLoginScreen = new Intent(MainActivity.this, LoginScreen.class);
                startActivity(intentLoginScreen);
                break;
            case R.id.btn_register:
                Intent intentRegisterScreen = new Intent(MainActivity.this, RegisterScreen.class);
                startActivity(intentRegisterScreen);

                break;
            default:
                break;
        }
    }
}
