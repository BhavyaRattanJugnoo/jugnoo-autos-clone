package com.example.rattan_bhavya.jugnooautos;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import rmn.androidscreenlibrary.ASSL;


public class LoginScreen extends Activity {

    TextView tvBack;
    TextView tvForgotPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);

        new ASSL(this, (ViewGroup) findViewById(R.id.root), 1134, 720, false);

        tvBack = (TextView) findViewById(R.id.tv_back);

        tvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tvForgotPassword = (TextView) findViewById(R.id.tv_forgot_password);

        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentForgotPassword = new Intent(LoginScreen.this, forgotPassword.class);
                startActivity(intentForgotPassword);
            }
        });
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_login:
                Intent intentHome = new Intent(LoginScreen.this, Home.class);
                startActivity(intentHome);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ASSL.closeActivity(findViewById(R.id.root));
    }
}
